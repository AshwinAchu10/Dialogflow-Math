module.exports.getGreetingTime = function(m) {
	var greeting = null;
	if(!m || !m.isValid()) { return; }
	var split_afternoon = 12
	var split_evening = 17
	var currentHour = parseFloat(m.format("HH"));
	if(currentHour >= split_afternoon && currentHour <= split_evening) {
		greeting = "AFTERNOON";
	} else if(currentHour >= split_evening) {
		greeting = "EVENING";
	} else {
		greeting = "MORNING";
	}
	return greeting;
}
