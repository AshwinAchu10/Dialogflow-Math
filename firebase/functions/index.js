const functions = require('firebase-functions');
const {
    dialogflow
} = require('actions-on-google');
let utility = require('./utils/utils')
let arithmeticCalculation = require('./intents/arithmeticCalculation')
const i18n = require('i18n');
const moment = require('moment');
const app = dialogflow({
    clientId: '422564065390-6k5fr29188rg8q0beigv29mshulqjm7p.apps.googleusercontent.com'
});

i18n.configure({
    locales: ['en-IN', 'en-US', 'es-ES', 'es-MX'],
    directory: __dirname + '/intents/locales',
    defaultLocale: 'en-US'
});

app.middleware((conv) => {
    i18n.setLocale('en-US');
    moment.locale('en-US');
});

app.intent('Default Welcome Intent', conv => {
    conv.ask(i18n.__('WELCOME', {
        'greeting': i18n.__(utility.getGreetingTime(moment()))
    }));
});

app.intent('ArithmeticCalculation', (conv, {
    arg1,
    arg2,
    symbol
}) => {
    return new Promise(function (resolve, reject) {
        let response = arithmeticCalculation.performMath(arg1, arg2, symbol)
        if (response) {
            conv.ask(`${i18n.__('RESULT_OF')} ${arg1} ${symbol} ${arg2} ${i18n.__('IS')} ${response}, ${i18n.__('ANYTHING_ELSE')}`);
            resolve();
        } else {
            conv.ask(`${i18n.__('PROBLEM_COMPUTING')}`);
            resolve();
        }
    });
});

app.intent('ArithmeticFunction', (conv, {
    operation
}) => {
    return new Promise(function (resolve, reject) {
        const parameters = {
            'mathoperation': operation
        };
        conv.contexts.set('mathoperation', 50, parameters);
        conv.ask(`${i18n.__('GIVE_NUMBERS')}`);
        resolve();
    });
});

app.intent('ArithmeticFunctionValues', (conv, {
    arg1,
    arg2
}) => {
    return new Promise(function (resolve, reject) {
        let contexts = conv.contexts.get('mathoperation');
        let mathoperation = contexts.parameters.mathoperation;
        let response = arithmeticCalculation.performMath(arg1, arg2, mathoperation)
        if (response) {
            conv.ask(`${i18n.__('RESULT_OF')} ${arg1} ${mathoperation} ${arg2} ${i18n.__('IS')} ${response}, ${i18n.__('ANYTHING_ELSE')}`);
            resolve();
        } else {
            conv.ask(`${i18n.__('PROBLEM_COMPUTING')}`);
            resolve();
        }
    });
});

app.intent('ArithmeticCalculationYesContinue', (conv) => {
    return new Promise(function (resolve, reject) {
        conv.ask(`${i18n.__('CONTINUE')}`);
        resolve();
    });
});

app.intent('ArithmeticCalculationNotContinue', (conv) => {
    return new Promise(function (resolve, reject) {
        conv.close(`${i18n.__('SEE_YOU_SOON')}`);
        resolve();
    });
});

app.intent('ArithmeticFunctionYesContinue', (conv) => {
    return new Promise(function (resolve, reject) {
        conv.ask(`${i18n.__('CONTINUE')}`);
        resolve();
    });
});

app.intent('ArithmeticFunctionNotContinue', (conv) => {
    return new Promise(function (resolve, reject) {
        conv.close(`${i18n.__('SEE_YOU_SOON')}`);
        resolve();
    });
});

exports.dialogflowFirebaseFulfillment = functions.https.onRequest(app);