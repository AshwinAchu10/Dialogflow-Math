let i18n = require('i18n')


i18n.configure({
    locales: ['en-IN', 'en-US', 'es-ES', 'es-MX'],
    directory: __dirname + '/locales',
    defaultLocale: 'es-MX'
});


module.exports.performMath = function (arg1, arg2, symbol) {
    return eval(`${arg1} ${symbol} ${arg2}`)
}