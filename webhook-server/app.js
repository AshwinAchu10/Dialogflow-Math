const express= require('express');
const bodyParser = require('body-parser');
const i18n = require('i18n');
const app =  express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

i18n.configure({
    locales: ['en-IN', 'en-US', 'es-ES', 'es-MX'],
    directory: __dirname + '/services/locales',
    defaultLocale: 'en-US'
});

require('./routes/apiRoutes')(app);

const PORT = process.env.PORT  || 3000;
app.listen(PORT  ,() =>  {
    console.log(`The express server is listening on ${PORT}`)
})