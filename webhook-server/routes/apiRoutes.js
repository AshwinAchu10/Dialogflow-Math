const welcomeService = require('../services/welcome');
const arithmeticService = require('../services/arithmeticOperation');
const {
    WebhookClient
} = require('dialogflow-fulfillment');

module.exports = (app) => {

    function callDialogflowAgent(request, response) {
        const agent = new WebhookClient({
            request: request,
            response: response
        });
        let intentMap = new Map();
        intentMap.set('Default Welcome Intent', welcomeService.welcome);
        intentMap.set('ArithmeticFunction', arithmeticService.arithmeticFunction);
        intentMap.set('ArithmeticFunctionValues', arithmeticService.arithmeticFunctionValues);
        intentMap.set('ArithmeticCalculation', arithmeticService.arithmeticCalculation);
        intentMap.set('ArithmeticCalculationYesContinue', arithmeticService.arithmeticContinue);
        intentMap.set('ArithmeticFunctionYesContinue', arithmeticService.arithmeticContinue);
        intentMap.set('ArithmeticCalculationNotContinue', arithmeticService.arithmeticNotContinue);
        intentMap.set('ArithmeticFunctionNotContinue', arithmeticService.arithmeticNotContinue);
        agent.handleRequest(intentMap);
    }

    app.post('/math-analyzer', (request, response) => {
        callDialogflowAgent(request, response);
    });
}