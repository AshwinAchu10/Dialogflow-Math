let i18n = require('i18n')
let util = require('../utils/util');

const performMath = (arg1, arg2, symbol) => {
    return eval(`${arg1} ${symbol} ${arg2}`)
}

exports.arithmeticContinue = (agent) => {
    agent.add(`${i18n.__('CONTINUE')}`);
}

exports.arithmeticNotContinue = (agent) => {
    util.clearContext(agent, ['operation']);
    agent.add(`${i18n.__('SEE_YOU_SOON')}`);
}

exports.arithmeticFunction = (agent) => {
    let {
        operation
    } = agent.parameters;
    agent.context.set({
        'name': 'operation',
        'lifespan': 5,
        'parameters': {
            'symbol': operation
        }
    });
    agent.add(`${i18n.__('GIVE_NUMBERS')}`);
}

exports.arithmeticFunctionValues = (agent) => {
    let {
        arg1,
        arg2
    } = agent.parameters;
    let symbol = agent.context.get('operation').parameters.symbol;
    agent.add(`${i18n.__('RESULT_OF')} ${arg1} ${symbol} ${arg2} ${i18n.__('IS')} ${performMath(arg1, arg2, symbol)}, ${i18n.__('ANYTHING_ELSE')}`);
}

exports.arithmeticCalculation = (agent) => {
    let {
        arg1,
        arg2,
        symbol
    } = agent.parameters;
    agent.add(`${i18n.__('RESULT_OF')} ${arg1} ${symbol} ${arg2} ${i18n.__('IS')} ${performMath(arg1, arg2, symbol)}, ${i18n.__('ANYTHING_ELSE')}`);
}