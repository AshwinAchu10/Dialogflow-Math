let i18n = require('i18n')
let moment = require('moment');
const util = require('../utils/util');


exports.welcome = (agent) => {
    agent.add(i18n.__('WELCOME', {
        'greeting': i18n.__(util.getGreetingTime(moment()))
    }));
}