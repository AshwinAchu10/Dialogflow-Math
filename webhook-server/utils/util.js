exports.getGreetingTime = (momentDate) => {
	var greeting = null;
	if (!momentDate || !momentDate.isValid()) {
		return;
	}
	var split_afternoon = 12
	var split_evening = 17
	var currentHour = parseFloat(momentDate.format("HH"));
	if (currentHour >= split_afternoon && currentHour <= split_evening) {
		greeting = "AFTERNOON";
	} else if (currentHour >= split_evening) {
		greeting = "EVENING";
	} else {
		greeting = "MORNING";
	}
	return greeting;
}


exports.clearContext = (agent, contexts) => {
	for (let i = 0; i < contexts.length; i++) {
		agent.context.delete(contexts[i]);
	}
}