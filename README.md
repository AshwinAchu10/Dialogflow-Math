---
# Math Dialogflow Agent
---
Clone the repository - `git clone https://github.com/AshwinAchu10/Dialogflow-Math.git`

go inside project - `cd Dialogflow-Math`

Go to firebase function to install dependencies which will act as a webhook for our dialogflow calls - `cd firebase/functions/`

Install dependencies using - `npm install`

Create a project agent in dialogflow and import the zip file inside the project folder name is `MathAnalyst.zip`

Now go to Fullfillment tab and enable fullfilment

Once done take a note of your project id from settings and then, switch to local folder

Make sure you are inside the `firebase` directory

Do firebase local login to depoy the code using the command - `firebase login`

Once authenticated, use the below command to deploy the function to firebase - `firebase deploy --project=<project_id>`

Yes, once deployed. You are done with configuration.

Test it in the simulator within dialogflow or Go to integration tab and select Web integration to test it in chat bot web view. 

Which is a URL something like below (Note - Its my project actual url) - `https://bot.dialogflow.com/0f650cc2-b33a-4133-bec3-ef610f127938`

---
#### Some Commands to Test

---
User - `hi`

User - `addition`

User - `subtraction`

User - `multiplication`

User - `division`

User - `5 6`

User - `4 and 8`

User - `what is 5 * 8`

User - `what is 8 - 4`

---
### Demo video is attached in the repository
---
